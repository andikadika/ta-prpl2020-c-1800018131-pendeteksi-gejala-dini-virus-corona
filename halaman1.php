<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form action="input_user.php" method="post">
		<div class="alert alert-light" role="alert">
  			<p><u>Identitas Diri</u></p>
		</div>
		
			<table width="1500">
				<tr >
					<td width="100">Nama</td>
					<td width="120"> </td>
					<td> <input type="text" class="form-control" placeholder="Nama" name="nama"></td>
				</tr>
				<tr>
					<td>Jenis  Kelamin</td>
					<td> </td>
					<td> <input type="text" class="form-control" placeholder="Jenis Kelamin" name="jenis_kelamin"></td>
				</tr>
				<tr>
					<td>Tanggal Lahir</td>
					<td> </td>
					<td> <div class="form-row">
							<div class="form-group col-md-4">
								<select class="form-control" name="tahun">
  					 
  									<option>tahun</option>
  										<?php
  									for ($y=date('Y'); $y>=1950 ; $y--) { 
  										?>
  										<option value="<?php echo $y  ?>"><?php echo $y  ?> </option>
  									
  									<?php
  								} ?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<select name="bulan" class="form-control">
  									<option>Bulan</option>
  										<?php
  									for ($m=0; $m<=11 ; $m++) { 
  										?>
  										<?php
  										$nama_bulan  = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember' );
  										?>
  										<option value="<?php echo $m  ?>"><?php echo $nama_bulan[$m]  ?> </option>
  									
  									<?php
  								} ?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<select name="tanggal" class="form-control">
  									<option>Tanggal</option>
  									<?php
  									for ($d=1; $d<=31 ; $d++) { 
  										?>
  										<option value="<?php echo $d  ?>"><?php echo $d  ?> </option>
  									
  									<?php
  								} ?>
  									
								</select>
							</div>
						</div>

					
				</td>
			</tr>

			<tr>
				<td>Nomer WA</td>
				<td> </td>
				<td><input type="text" class="form-control" placeholder="Nomer WA / HP" name="nomer_wa"></td>
			</tr>

		</table>

		<div class="alert alert-light" role="alert">
  			<p><u>Alamat Sekarang Anda Tinggal</u></p>
		</div>
			<div class="form-group col-md-4">
		<tr>
			<td>Provinsi</td>
			<td></td>
		</tr>
		<tr>
			<td><input type="text" class="form-control" placeholder="Provinsi" name="provinsi" width="20"></td>
			<td></td>
		</tr>
		<tr>
			<td>Kabupaten</td>
			<td></td>
		</tr>
		<tr>
			<td><input type="text" class="form-control" placeholder="Kabupaten" name="kabupaten" width="20"></td>
			<td></td>
		</tr>
		<tr>
			<td>Kecamatan</td>
			<td></td>
		</tr>
		<tr>
			<td><input type="text" class="form-control" placeholder="Kecamatan" name="kecamatan" width="20"></td>
		</tr>

		</div>
		
		<button type="submit" class="btn btn-primary">Check</button>
		</form>
</body>
</html>